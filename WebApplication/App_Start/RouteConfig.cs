﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("GetList",
                            "DeliveryDetails/GetList/{id1}/{id2}",
                            new { controller = "DeliveryDetails", action = "GetList"});
            routes.MapRoute("GetPOList",
                            "RequisitionDetails/GetPOList/{id1}/{id2}",
                            new { controller = "RequisitionDetails", action = "GetPOList" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
