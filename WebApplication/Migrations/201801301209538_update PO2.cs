namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePO2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseOrders", "PurchaseRequest_ID", "dbo.PurchaseRequests");
            DropIndex("dbo.PurchaseOrders", new[] { "PurchaseRequest_ID" });
            AddColumn("dbo.PurchaseOrders", "PurchaseRequest_ID1", c => c.Int());
            CreateIndex("dbo.PurchaseOrders", "PurchaseRequest_ID1");
            AddForeignKey("dbo.PurchaseOrders", "PurchaseRequest_ID1", "dbo.PurchaseRequests", "ID");
            DropColumn("dbo.PurchaseOrders", "PrNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseOrders", "PrNumber", c => c.String());
            DropForeignKey("dbo.PurchaseOrders", "PurchaseRequest_ID1", "dbo.PurchaseRequests");
            DropIndex("dbo.PurchaseOrders", new[] { "PurchaseRequest_ID1" });
            DropColumn("dbo.PurchaseOrders", "PurchaseRequest_ID1");
            CreateIndex("dbo.PurchaseOrders", "PurchaseRequest_ID");
            AddForeignKey("dbo.PurchaseOrders", "PurchaseRequest_ID", "dbo.PurchaseRequests", "ID");
        }
    }
}
