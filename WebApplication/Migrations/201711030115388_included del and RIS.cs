namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includeddelandRIS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PurchaseOrderDetailID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        DateRecorded = c.DateTime(nullable: false),
                        RecordedBy = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Requisitions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PurchaseOrderDetailID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        DateRecorded = c.DateTime(nullable: false),
                        RecordedBy = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Requisitions");
            DropTable("dbo.Deliveries");
        }
    }
}
