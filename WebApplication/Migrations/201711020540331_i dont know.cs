namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idontknow : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Categories", new[] { "CategoryName" });
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PrNumber = c.Int(nullable: false),
                        PoNumber = c.Int(nullable: false),
                        SupplierID = c.Int(nullable: false),
                        CostCenterID = c.Int(nullable: false),
                        Purpose = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CostCenters", t => t.CostCenterID, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID, cascadeDelete: true)
                .Index(t => t.SupplierID)
                .Index(t => t.CostCenterID);
            
            CreateTable(
                "dbo.CostCenters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PurchaseOrderDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PurchaseOrderID = c.Int(nullable: false),
                        StockID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderID, cascadeDelete: true)
                .ForeignKey("dbo.Stocks", t => t.StockID, cascadeDelete: true)
                .Index(t => t.PurchaseOrderID)
                .Index(t => t.StockID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseOrders", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseOrderDetails", "StockID", "dbo.Stocks");
            DropForeignKey("dbo.PurchaseOrderDetails", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "CostCenterID", "dbo.CostCenters");
            DropIndex("dbo.PurchaseOrderDetails", new[] { "StockID" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "PurchaseOrderID" });
            DropIndex("dbo.PurchaseOrders", new[] { "CostCenterID" });
            DropIndex("dbo.PurchaseOrders", new[] { "SupplierID" });
            DropTable("dbo.PurchaseOrderDetails");
            DropTable("dbo.CostCenters");
            DropTable("dbo.PurchaseOrders");
            CreateIndex("dbo.Categories", "CategoryName", unique: true);
        }
    }
}
