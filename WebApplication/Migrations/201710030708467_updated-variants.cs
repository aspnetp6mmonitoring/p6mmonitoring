namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedvariants : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplyVariants", "Variants", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplyVariants", "Variants");
        }
    }
}
