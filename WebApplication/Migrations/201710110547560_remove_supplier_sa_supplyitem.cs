namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_supplier_sa_supplyitem : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.SupplyItems", name: "SupplierID", newName: "Supplier_ID");
            RenameIndex(table: "dbo.SupplyItems", name: "IX_SupplierID", newName: "IX_Supplier_ID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.SupplyItems", name: "IX_Supplier_ID", newName: "IX_SupplierID");
            RenameColumn(table: "dbo.SupplyItems", name: "Supplier_ID", newName: "SupplierID");
        }
    }
}
