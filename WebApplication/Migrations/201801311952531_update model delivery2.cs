namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatemodeldelivery2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Deliveries", "PurchaseOrders_ID", "dbo.PurchaseOrders");
            DropIndex("dbo.Deliveries", new[] { "PurchaseOrders_ID" });
            RenameColumn(table: "dbo.Deliveries", name: "PurchaseOrders_ID", newName: "PurchaseOrderID");
            AlterColumn("dbo.Deliveries", "PurchaseOrderID", c => c.Int(nullable: false));
            CreateIndex("dbo.Deliveries", "PurchaseOrderID");
            AddForeignKey("dbo.Deliveries", "PurchaseOrderID", "dbo.PurchaseOrders", "ID", cascadeDelete: true);
            DropColumn("dbo.Deliveries", "PoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deliveries", "PoID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Deliveries", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropIndex("dbo.Deliveries", new[] { "PurchaseOrderID" });
            AlterColumn("dbo.Deliveries", "PurchaseOrderID", c => c.Int());
            RenameColumn(table: "dbo.Deliveries", name: "PurchaseOrderID", newName: "PurchaseOrders_ID");
            CreateIndex("dbo.Deliveries", "PurchaseOrders_ID");
            AddForeignKey("dbo.Deliveries", "PurchaseOrders_ID", "dbo.PurchaseOrders", "ID");
        }
    }
}
