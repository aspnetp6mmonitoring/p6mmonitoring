namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedfield : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PurchaseOrders", "PrNumber", c => c.String(nullable: false));
            AlterColumn("dbo.PurchaseOrders", "PoNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PurchaseOrders", "PoNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.PurchaseOrders", "PrNumber", c => c.Int(nullable: false));
        }
    }
}
