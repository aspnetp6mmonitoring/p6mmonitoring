// <auto-generated />
namespace WebApplication.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class updateddisplayfieldforbrandname : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateddisplayfieldforbrandname));
        
        string IMigrationMetadata.Id
        {
            get { return "201710040139152_updated display field for brand name"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
