namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePO1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrders", "PrNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "PrNumber");
        }
    }
}
