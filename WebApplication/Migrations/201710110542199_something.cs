namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class something : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.SupplyItems", name: "Category_ID", newName: "CategoryID");
            RenameIndex(table: "dbo.SupplyItems", name: "IX_Category_ID", newName: "IX_CategoryID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.SupplyItems", name: "IX_CategoryID", newName: "IX_Category_ID");
            RenameColumn(table: "dbo.SupplyItems", name: "CategoryID", newName: "Category_ID");
        }
    }
}
