namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabasemodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseRequests", "PrNumber", c => c.String());
            DropColumn("dbo.PurchaseRequests", "PrID");
            DropColumn("dbo.Deliveries", "PoNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deliveries", "PoNumber", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseRequests", "PrID", c => c.Int());
            DropColumn("dbo.PurchaseRequests", "PrNumber");
        }
    }
}
