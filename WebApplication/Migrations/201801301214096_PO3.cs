namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PO3 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PurchaseOrders", "PurchaseRequest_ID");
            RenameColumn(table: "dbo.PurchaseOrders", name: "PurchaseRequest_ID1", newName: "PurchaseRequest_ID");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_PurchaseRequest_ID1", newName: "IX_PurchaseRequest_ID");
            AddColumn("dbo.PurchaseOrders", "PrID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "PrID");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_PurchaseRequest_ID", newName: "IX_PurchaseRequest_ID1");
            RenameColumn(table: "dbo.PurchaseOrders", name: "PurchaseRequest_ID", newName: "PurchaseRequest_ID1");
            AddColumn("dbo.PurchaseOrders", "PurchaseRequest_ID", c => c.Int());
        }
    }
}
