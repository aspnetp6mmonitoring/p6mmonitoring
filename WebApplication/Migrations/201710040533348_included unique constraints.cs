namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includeduniqueconstraints : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Brands", "BrandName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Supplies", "Name", c => c.String(maxLength: 300));
            AlterColumn("dbo.Categories", "CategoryName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Suppliers", "SupplierName", c => c.String(maxLength: 300));
            CreateIndex("dbo.Brands", "BrandName", unique: true);
            CreateIndex("dbo.Supplies", "Name", unique: true);
            CreateIndex("dbo.Categories", "CategoryName", unique: true);
            CreateIndex("dbo.Suppliers", "SupplierName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Suppliers", new[] { "SupplierName" });
            DropIndex("dbo.Categories", new[] { "CategoryName" });
            DropIndex("dbo.Supplies", new[] { "Name" });
            DropIndex("dbo.Brands", new[] { "BrandName" });
            AlterColumn("dbo.Suppliers", "SupplierName", c => c.String());
            AlterColumn("dbo.Categories", "CategoryName", c => c.String());
            AlterColumn("dbo.Supplies", "Name", c => c.String());
            AlterColumn("dbo.Brands", "BrandName", c => c.String(nullable: false));
        }
    }
}
