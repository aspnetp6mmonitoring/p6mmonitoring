namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addvariantfieldinsuppliesentity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SupplyVariants", "Supplies_ID", "dbo.SupplyItems");
            DropIndex("dbo.SupplyVariants", new[] { "Supplies_ID" });
            AddColumn("dbo.SupplyItems", "Variant", c => c.String());
            DropTable("dbo.SupplyVariants");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SupplyVariants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplyID = c.Int(nullable: false),
                        Variants = c.String(),
                        Supplies_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.SupplyItems", "Variant");
            CreateIndex("dbo.SupplyVariants", "Supplies_ID");
            AddForeignKey("dbo.SupplyVariants", "Supplies_ID", "dbo.SupplyItems", "ID");
        }
    }
}
