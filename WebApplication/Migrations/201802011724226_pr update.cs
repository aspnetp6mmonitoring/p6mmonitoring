namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prupdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PurchaseRequests", "PrNumber", c => c.String(maxLength: 9));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PurchaseRequests", "PrNumber", c => c.String());
        }
    }
}
