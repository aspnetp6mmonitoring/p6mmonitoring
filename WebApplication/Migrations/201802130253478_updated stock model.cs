namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedstockmodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stocks", "StockNumber", c => c.String());
            AddColumn("dbo.Stocks", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Stocks", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Stocks", "Description", c => c.String(nullable: false));
            DropColumn("dbo.Stocks", "Name");
            DropColumn("dbo.Stocks", "StockNumber");
        }
    }
}
