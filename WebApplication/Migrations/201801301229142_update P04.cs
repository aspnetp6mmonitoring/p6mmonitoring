namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateP04 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.PurchaseOrders", name: "PurchaseRequest_ID", newName: "PurchaseRequestID");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_PurchaseRequest_ID", newName: "IX_PurchaseRequestID");
            DropColumn("dbo.PurchaseOrders", "PrID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseOrders", "PrID", c => c.Int());
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_PurchaseRequestID", newName: "IX_PurchaseRequest_ID");
            RenameColumn(table: "dbo.PurchaseOrders", name: "PurchaseRequestID", newName: "PurchaseRequest_ID");
        }
    }
}
