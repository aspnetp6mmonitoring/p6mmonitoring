namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedsupplyattributeclass : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SupplyAttibutes", "SupplyVariant_Id", "dbo.SupplyVariants");
            DropIndex("dbo.SupplyAttibutes", new[] { "SupplyVariant_Id" });
            DropTable("dbo.SupplyAttibutes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SupplyAttibutes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        value = c.String(),
                        SupplyVariant_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.SupplyAttibutes", "SupplyVariant_Id");
            AddForeignKey("dbo.SupplyAttibutes", "SupplyVariant_Id", "dbo.SupplyVariants", "Id");
        }
    }
}
