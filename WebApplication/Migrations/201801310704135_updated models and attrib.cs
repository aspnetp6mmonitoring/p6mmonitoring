namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedmodelsandattrib : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requisitions", "Delivery_ID", "dbo.Deliveries");
            DropIndex("dbo.Requisitions", new[] { "Delivery_ID" });
            AddColumn("dbo.Requisitions", "PurchaseOrderID", c => c.Int());
            CreateIndex("dbo.Requisitions", "PurchaseOrderID");
            AddForeignKey("dbo.Requisitions", "PurchaseOrderID", "dbo.PurchaseOrders", "ID");
            DropColumn("dbo.Requisitions", "Delivery_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requisitions", "Delivery_ID", c => c.Int());
            DropForeignKey("dbo.Requisitions", "PurchaseOrderID", "dbo.PurchaseOrders");
            DropIndex("dbo.Requisitions", new[] { "PurchaseOrderID" });
            DropColumn("dbo.Requisitions", "PurchaseOrderID");
            CreateIndex("dbo.Requisitions", "Delivery_ID");
            AddForeignKey("dbo.Requisitions", "Delivery_ID", "dbo.Deliveries", "ID");
        }
    }
}
