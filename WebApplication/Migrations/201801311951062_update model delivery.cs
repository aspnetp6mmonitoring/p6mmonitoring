namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatemodeldelivery : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Deliveries", "Quantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deliveries", "Quantity", c => c.Int(nullable: false));
        }
    }
}
