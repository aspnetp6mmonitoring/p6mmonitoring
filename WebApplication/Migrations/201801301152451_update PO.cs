namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePO : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PurchaseOrders", "PrID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseOrders", "PrID", c => c.Int(nullable: false));
        }
    }
}
