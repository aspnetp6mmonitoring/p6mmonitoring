// <auto-generated />
namespace WebApplication.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class updatemodeldelivery2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updatemodeldelivery2));
        
        string IMigrationMetadata.Id
        {
            get { return "201801311952531_update model delivery2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
