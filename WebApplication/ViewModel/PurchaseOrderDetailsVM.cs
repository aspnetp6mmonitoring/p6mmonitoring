﻿using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.ViewModel
{
    public class PurchaseOrderDetailsVM
    {
        public IEnumerable<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public IEnumerable<PurchaseOrder> PurchaseOrders { get; set; }
        
    }
}