﻿using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.ViewModel
{
    public class DeliveryDetailsVM
    {
        public IEnumerable<PurchaseOrder> PurchaseOrders { get; set; }
        public IEnumerable<Delivery> Deliveries { get; set; }
        public IEnumerable<DeliveryDetail> DeliveryDetails { get; set; }
    }
}