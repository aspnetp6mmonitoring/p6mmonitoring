﻿using System.Collections.Generic;
using WebApplication.Models;

namespace WebApplication.ViewModel
{
    public class DeliveryDetailsViewModel
    {

        public IEnumerable<PurchaseOrder> PurchaseOrders { get; set; }
        public IEnumerable<Delivery> Deliveries { get; set; }
        public IEnumerable<DeliveryDetail> DeliveryDetails { get; set; }
        public IEnumerable<Stock> Stocks { get; set; }
        public IEnumerable<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public IEnumerable<Unit> Units { get; set; }

    }
}