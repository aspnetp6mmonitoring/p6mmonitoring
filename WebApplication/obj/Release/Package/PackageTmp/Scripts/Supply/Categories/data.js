﻿$(function () {
    var table = $('#categories-all').DataTable({
        drawCallback: function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });
        }
    });

    $('.search-panel .dropdown-menu').find('a').click(function (e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#", "");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
    });


    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        // other options
    });


    $("#form_add_category").validate({
        errorElement: "span",
        errorClass: "help-block text-red",
        submitHandler: function (vform) {
            vform.submit();
        },
        rules: {
            CategoryName: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            CategoryName: {
                required: "Not submitted! Text field cannot be empty.",
                minlength: jQuery.validator.format("At least {0} characters required!")
            }
        }
    });


});