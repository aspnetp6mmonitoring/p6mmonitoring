﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class PurchaseOrderDetailsController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        // GET: PurchaseOrderDetails
        public async Task<ActionResult> Index()
        {
            var purchaseOrderDetails = db.PurchaseOrderDetails.Include(p => p.PurchaseOrder).Include(p => p.Stock);
            return View(await purchaseOrderDetails.ToListAsync());
        }

        // GET: PurchaseOrderDetails/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetail purchaseOrderDetail = await db.PurchaseOrderDetails.FindAsync(id);
            if (purchaseOrderDetail == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrderDetail);
        }

        // GET: PurchaseOrderDetails/Create
        public ActionResult Create()
        {
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description");
            return View();
        }

        // POST: PurchaseOrderDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PurchaseOrderID,StockID,Quantity")] PurchaseOrderDetail purchaseOrderDetail)
        {
            if (ModelState.IsValid)
            {
                db.PurchaseOrderDetails.Add(purchaseOrderDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", purchaseOrderDetail.PurchaseOrderID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", purchaseOrderDetail.StockID);
            return View(purchaseOrderDetail);
        }

        // GET: PurchaseOrderDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetail purchaseOrderDetail = await db.PurchaseOrderDetails.FindAsync(id);
            if (purchaseOrderDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", purchaseOrderDetail.PurchaseOrderID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", purchaseOrderDetail.StockID);
            return View(purchaseOrderDetail);
        }

        // POST: PurchaseOrderDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PurchaseOrderID,StockID,Quantity")] PurchaseOrderDetail purchaseOrderDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseOrderDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", purchaseOrderDetail.PurchaseOrderID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", purchaseOrderDetail.StockID);
            return View(purchaseOrderDetail);
        }

        // GET: PurchaseOrderDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrderDetail purchaseOrderDetail = await db.PurchaseOrderDetails.FindAsync(id);
            if (purchaseOrderDetail == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrderDetail);
        }

        // POST: PurchaseOrderDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PurchaseOrderDetail purchaseOrderDetail = await db.PurchaseOrderDetails.FindAsync(id);
            db.PurchaseOrderDetails.Remove(purchaseOrderDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
