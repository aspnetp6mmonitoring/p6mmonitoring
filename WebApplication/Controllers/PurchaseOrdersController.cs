﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class PurchaseOrdersController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        [AjaxOnly]
        public PartialViewResult GetList()
        {
            ViewBag.PurchaseRequests = db.PurchaseRequests.ToList();
            return PartialView("_purchaseordersList", db.PurchaseOrders.ToList());
        }
        [AjaxOnly]
        public PartialViewResult GetListDetail(int? id)
        {
            ViewBag.Stocks = db.Stocks.ToList();
            
            
            return PartialView("_purchaseorderdetails", db.PurchaseOrderDetails.Where(c => c.PurchaseOrderID == id).ToList());
        }
        [AjaxOnly]
        public PartialViewResult GetListDelivery(int? id)
        {
            ViewBag.Deliveries = db.Deliveries.ToList();


            return PartialView("_deliveryList", db.Deliveries.Where(c => c.PurchaseOrderID == id).ToList());
        }
        [AjaxOnly]
        public PartialViewResult GetListRequisition(int? id)
        {
            ViewBag.Requisitions = db.Requisitions.ToList();


            return PartialView("_requisitionsList", db.Requisitions.Where(c => c.PurchaseOrderID == id).ToList());
        }
        // GET: PR
        public ActionResult Index()
        {
            ViewBag.PurchaseRequest = db.PurchaseRequests.Include(d => d.CostCenter).ToList();
            return View();
        }

        // GET: PurchaseOrders/Requisitions/5
        public async Task<ActionResult> Requisitions(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PO = purchaseOrder;

            return View();
        }

        // GET: PurchaseOrders/Deliveries/5
        public async Task<ActionResult> Deliveries(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PO = purchaseOrder;
            
            return View();
        }
        public async Task<ActionResult> DeliveryDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = await db.Deliveries.FindAsync(id);
            
            if (delivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.Delivery = delivery;

            return View();
        }
        // GET: PurchaseOrders/PurchaseOrderDetails/5
        public async Task<ActionResult> PurchaseOrderDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PO = purchaseOrder;
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description");
            return View();
        }

        // GET: PurchaseOrders/Create
        public ActionResult Create()
        {
            ViewBag.PurchaseRequestID = new SelectList(db.PurchaseRequests, "ID", "PrNumber");
            return View();
        }

        // POST: PurchaseOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PoNumber,SupplierName,SupplierAddress,Purpose,DateRecorded,PurchaseRequestID")] PurchaseOrder purchaseOrder)
        {
            if (ModelState.IsValid)
            {
                db.PurchaseOrders.Add(purchaseOrder);
                await db.SaveChangesAsync();
                return RedirectToAction("PurchaseOrderDetails",new { id = purchaseOrder.ID});
            }

            ViewBag.PurchaseRequestID = new SelectList(db.PurchaseRequests, "ID", "PrNumber", purchaseOrder.PurchaseRequestID);
            return View(purchaseOrder);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> CheckDetails([Bind(Include = "ID,PurchaseOrderID,StockID,Quantity")] PurchaseOrderDetail purchaseOrderDetail)
        {
            var ans = "";
            var podetails = db.PurchaseOrderDetails.FirstOrDefault(k => k.PurchaseOrderID == purchaseOrderDetail.PurchaseOrderID && k.StockID == purchaseOrderDetail.StockID);
            if (ReferenceEquals(podetails, null))
            {
                db.PurchaseOrderDetails.Add(purchaseOrderDetail);
                await db.SaveChangesAsync();
            }
            else
            {
                ans = "0";
            }
            return ans;

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> CreateDetails([Bind(Include = "ID,PurchaseOrderID,StockID,Quantity")] PurchaseOrderDetail purchaseOrderDetail)
        {
            var ans = "";
            if (ModelState.IsValid)
            {
                var podetails = db.PurchaseOrderDetails.FirstOrDefault(k => k.PurchaseOrderID == purchaseOrderDetail.PurchaseOrderID && k.StockID == purchaseOrderDetail.StockID);
                
                    podetails.Quantity = podetails.Quantity + purchaseOrderDetail.Quantity;
                    await db.SaveChangesAsync();

                ans = "done";
                
                
            }
            return ans;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateDeliveries([Bind(Include = "ID,PurchaseOrderID,DateRecorded")] Delivery delivery)
        {
            if (ModelState.IsValid)
            {
                db.Deliveries.Add(delivery);
                await db.SaveChangesAsync();
                return RedirectToAction("Deliveries", new { id = delivery.PurchaseOrderID });
            }

            
            return View(delivery);
        }

        // POST: Requisitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> CreateRequisitions([Bind(Include = "ID,ReNumber,DateRecorded,PurchaseOrderID")] Requisition requisition)
        {
            var ans = "";
            Delivery del = db.Deliveries.FirstOrDefault(c => c.PurchaseOrderID == requisition.PurchaseOrderID);
            
            if (!ReferenceEquals(del, null))
            {
                db.Requisitions.Add(requisition);
                await db.SaveChangesAsync();
            }
            else
            {
                ans = "0";
            }
            return ans;
        }


        // GET: PurchaseOrders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.PurchaseRequestID = new SelectList(db.PurchaseRequests, "ID", "PrNumber", purchaseOrder.PurchaseRequestID);
            return View(purchaseOrder);
        }

        // POST: PurchaseOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PoNumber,SupplierName,SupplierAddress,Purpose,DateRecorded,PurchaseRequestID")] PurchaseOrder purchaseOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseOrder).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PurchaseRequestID = new SelectList(db.PurchaseRequests, "ID", "PrNumber", purchaseOrder.PurchaseRequestID);
            return View(purchaseOrder);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDetails([Bind(Include = "ID,PurchaseOrderID,StockID,Quantity")] PurchaseOrderDetail purchaseOrderDetail)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(purchaseOrderDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("PurchaseOrderDetails", new { id = purchaseOrderDetail.PurchaseOrderID });
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description");
            return View(purchaseOrderDetail);
        }

        // GET: PurchaseOrders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // POST: PurchaseOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PurchaseOrder purchaseOrder = await db.PurchaseOrders.FindAsync(id);
            db.PurchaseOrders.Remove(purchaseOrder);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // POST: PurchaseOrderDetails/Delete/5
        [HttpPost, ActionName("DeleteDetails")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed1(int id, string poID)
        {
            PurchaseOrderDetail s = await db.PurchaseOrderDetails.FindAsync(id);
            db.PurchaseOrderDetails.Remove(s);
            await db.SaveChangesAsync();
            return RedirectToAction("PurchaseOrderDetails", new { id = poID });
        }
        // POST: PurchaseOrderDetails/Delete/5
        [HttpPost, ActionName("DeleteDeliveries")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed2(int id, string poID)
        {
            Delivery s = await db.Deliveries.FindAsync(id);
            db.Deliveries.Remove(s);
            await db.SaveChangesAsync();
            return RedirectToAction("Deliveries", new { id = poID });
        }
        // POST: Requisitions/Delete/5
        [HttpPost, ActionName("DeleteRequisitions")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed3(int id, string poID)
        {
            Requisition requisition = await db.Requisitions.FindAsync(id);
            db.Requisitions.Remove(requisition);
            await db.SaveChangesAsync();
            return RedirectToAction("Requisitions", new { id = poID });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
