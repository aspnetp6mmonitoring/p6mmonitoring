﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class RequisitionsController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        [AjaxOnly]
        public PartialViewResult GetList()
        {
            ViewBag.PurchaseRequests = db.PurchaseRequests.ToList();
            return PartialView("_requisitionsList", db.Requisitions.ToList());
        }

        // GET: Requisitions
        public ActionResult Index()
        {
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            ViewBag.Requisitions = db.Requisitions.Include(r => r.PurchaseOrder);
            return View();
        }

        // GET: Requisitions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requisition requisition = await db.Requisitions.FindAsync(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            return View(requisition);
        }

        // GET: Requisitions/Create
        public ActionResult Create()
        {
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            return View();
        }

        // POST: Requisitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> Create([Bind(Include = "ID,ReNumber,DateRecorded,PurchaseOrderID")] Requisition requisition)
        {
            string ans = "";
            if (ModelState.IsValid)
            {
                db.Requisitions.Add(requisition);
                var poid_in_delivery = db.Deliveries.FirstOrDefault(c => c.PurchaseOrderID == requisition.PurchaseOrderID );
                if (!ReferenceEquals(poid_in_delivery, null))
                {
                    db.Requisitions.Add(requisition);
                    await db.SaveChangesAsync();
                }else
                {
                    ans = "0";
                }
                return ans;
            }

            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", requisition.PurchaseOrderID);
            return ans;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create1([Bind(Include = "ID,ReNumber,DateRecorded,PurchaseOrderID")] Requisition requisition)
        {
            return RedirectToAction("Deliveries");
        }

        // GET: Requisitions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requisition requisition = await db.Requisitions.FindAsync(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", requisition.PurchaseOrderID);
            return View(requisition);
        }

        // POST: Requisitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ReNumber,DateRecorded,PurchaseOrderID")] Requisition requisition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requisition).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", requisition.PurchaseOrderID);
            return View(requisition);
        }

        // GET: Requisitions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requisition requisition = await db.Requisitions.FindAsync(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            return View(requisition);
        }

        // POST: Requisitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Requisition requisition = await db.Requisitions.FindAsync(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            db.Requisitions.Remove(requisition);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
