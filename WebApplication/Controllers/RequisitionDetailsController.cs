﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class RequisitionDetailsController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        // GET: RequisitionDetails
        
        public async Task<ActionResult> Index(int? id)
        {
            Requisition requisition = await db.Requisitions.FindAsync(id);
            ViewBag.Requisition = requisition;
            return View();
        }
        [AjaxOnly]
        public async Task<PartialViewResult> GetPOList(int? id1, int id2)
        {

            ViewBag.reqID = id2;
            return PartialView("_purchaseorderdetails", db.PurchaseOrderDetails.Where(c => c.PurchaseOrderID == id1).ToList());
        }
        [AjaxOnly]
        public async Task<PartialViewResult> GetList1(int? id)
        {
            Requisition requisition = await db.Requisitions.FindAsync(id);
            ViewBag.Requisition = requisition;

            return PartialView("_requisitiondetailsList", db.RequisitionDetails.Where(c => c.RequisitionID == id).ToList());
        }
        
        
        // GET: RequisitionDetails/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RequisitionDetail requisitionDetail = await db.RequisitionDetails.FindAsync(id);
            if (requisitionDetail == null)
            {
                return HttpNotFound();
            }
            return View(requisitionDetail);
        }

        // GET: RequisitionDetails/Create
        public ActionResult Create()
        {
            ViewBag.RequisitionID = new SelectList(db.Requisitions, "ID", "ReNumber");
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description");
            return View();
        }

        // POST: RequisitionDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> Create([Bind(Include = "ID,RequisitionID,StockID,Quantity")] RequisitionDetail requisitionDetail)
        {
            var ans = "";
            var query = db.RequisitionDetails.FirstOrDefault(i => i.RequisitionID == requisitionDetail.RequisitionID && i.StockID == requisitionDetail.StockID);
            if(ReferenceEquals(query, null))
            { 
                db.RequisitionDetails.Add(requisitionDetail);
                await db.SaveChangesAsync();

                
            }
            else
            {
                ans = "0";
            }
            return ans;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> Create1([Bind(Include = "ID,RequisitionID,StockID,Quantity")] RequisitionDetail requisitionDetail)
        {
            var ans = "";
            var query = db.RequisitionDetails.FirstOrDefault(i => i.RequisitionID == requisitionDetail.RequisitionID && i.StockID == requisitionDetail.StockID);

            query.Quantity = query.Quantity + requisitionDetail.Quantity;
            await db.SaveChangesAsync();


            return "done";
        }


        // GET: RequisitionDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RequisitionDetail requisitionDetail = await db.RequisitionDetails.FindAsync(id);
            if (requisitionDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.RequisitionID = new SelectList(db.Requisitions, "ID", "ReNumber", requisitionDetail.RequisitionID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", requisitionDetail.StockID);
            return View(requisitionDetail);
        }

        // POST: RequisitionDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,RequisitionID,StockID,Quantity")] RequisitionDetail requisitionDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requisitionDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.RequisitionID = new SelectList(db.Requisitions, "ID", "ReNumber", requisitionDetail.RequisitionID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", requisitionDetail.StockID);
            return View(requisitionDetail);
        }

        // GET: RequisitionDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RequisitionDetail requisitionDetail = await db.RequisitionDetails.FindAsync(id);
            if (requisitionDetail == null)
            {
                return HttpNotFound();
            }
            return View(requisitionDetail);
        }

        // POST: RequisitionDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int id1)
        {
            RequisitionDetail requisitionDetail = await db.RequisitionDetails.FindAsync(id);
            db.RequisitionDetails.Remove(requisitionDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", new { id = id1 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
