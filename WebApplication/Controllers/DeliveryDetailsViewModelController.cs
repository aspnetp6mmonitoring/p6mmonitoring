﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.ViewModel;
using System.Linq;
using System;

namespace WebApplication.Controllers
{
    public class DeliveryDetailsViewModelController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        public ActionResult Index()
        {
            List<DeliveryDetailsViewModel> DeliveryDetails = new List<DeliveryDetailsViewModel>();

            //query is generated using LinqPAD 5
            var datalist = (db.Deliveries.Join(db.DeliveryDetails,d => (Int32?)(d.ID),dd => dd.DeliveryID,(d, dd) =>
                            new{PurchaseOrderID = d.PurchaseOrderID,StockID = dd.StockID,Quantity = dd.Quantity})).ToList();

            foreach (var item in datalist)
            {
                DeliveryDetailsViewModel rdvm = new DeliveryDetailsViewModel();
                rdvm.PurchaseOrderID = item.PurchaseOrderID;
                rdvm.StockID = item.StockID;
                rdvm.Quantity = item.Quantity;

                DeliveryDetails.Add(rdvm);
            }

            //return model/list
            return View(DeliveryDetails);
        }
    }
}
    