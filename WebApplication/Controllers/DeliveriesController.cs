﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class DeliveriesController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        // GET: Deliveries
        public ActionResult Index()
        {
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            ViewBag.deliveries = db.Deliveries.Include(d => d.PurchaseOrders);
            return View();
        }

        [AjaxOnly]
        public PartialViewResult GetList()
        {
            ViewBag.PurchaseOrders = db.PurchaseOrders.ToList();
            return PartialView("_deliveriesList", db.Deliveries.ToList());
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, string poID)
        {
            Delivery s = await db.Deliveries.FindAsync(id);
            db.Deliveries.Remove(s);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Deliveries/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = await db.Deliveries.FindAsync(id);
            if (delivery == null)
            {
                return HttpNotFound();
            }
            return View(delivery);
        }

        // GET: Deliveries/Create
        public ActionResult Create()
        {
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber");
            return View();
        }

        // POST: Deliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PurchaseOrderID,DateRecorded")] Delivery delivery)
        {
            if (ModelState.IsValid)
            {
                db.Deliveries.Add(delivery);
                await db.SaveChangesAsync();
                return RedirectToAction("Index","DeliveryDetails", new { id = delivery.ID }  );
            }

            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", delivery.PurchaseOrderID);
            return View(delivery);
        }

        // GET: Deliveries/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Delivery delivery = await db.Deliveries.FindAsync(id);
            if (delivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", delivery.PurchaseOrderID);
            return View(delivery);
        }

        // POST: Deliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PurchaseOrderID,DateRecorded")] Delivery delivery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(delivery).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PurchaseOrderID = new SelectList(db.PurchaseOrders, "ID", "PoNumber", delivery.PurchaseOrderID);
            return View(delivery);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
