﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class StocksController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        // GET: Stocks
        [AjaxOnly]
        public PartialViewResult GetList()
        {
            ViewBag.Units = db.Units.ToList();
            return PartialView("_unitsListStocks", db.Stocks.ToList());
        }

        // GET: Stocks
        public ActionResult Index()
        {
  
            ViewBag.UnitID = new SelectList(db.Units, "ID", "UnitName");
            return View();
        }


        //override
       
        public void UpdateContent()
        {
            List<Stock> stocks = db.Stocks.ToList();
            List<Stock> updateStocks = new List<Stock>();


            foreach (Stock s in stocks)
            {
                var description = s.Description;
                var descriptionArray = description.Split(',');
                var name = descriptionArray?[0] ?? description.ToUpper();

                if (!ReferenceEquals(descriptionArray[0], null))
                {
                    description = String.Join(", ", descriptionArray, 1, descriptionArray.Length - 1);
                }

                var stockNumberPrefix = "";


                
                //for stock number
                stockNumberPrefix = CleanString(name);
                   

                var stockCount = updateStocks.Where(c => c.Name == name)?.Count();
                stockCount++;

                var stockNumber = stockNumberPrefix + "-" + stockCount;

                Stock _stock = db.Stocks.Find(s.ID);
                _stock.StockNumber = stockNumber.Trim();
                _stock.Name = name.Trim();
                _stock.Description = description.Trim();
         

                updateStocks.Add(_stock);

                if (ModelState.IsValid)
                {
                    db.Entry(_stock).State = EntityState.Modified;

                    db.SaveChanges();

                }
                else
                {
                    Console.Write("ambot!");
                }
                
            }
            
        }


        protected string CleanString(string n)
        {
            if (n != null || n != "")
            {
                if (n.Length <= 4)
                {
                    //for stock number
                    return n.ToUpper();
                }
                else
                {
                    string vowels = "aeiouy";
                    string name = n.ToLower();

                    name = new string(name.Where(c => !vowels.Contains(c)).ToArray());
                    name = name.Replace(" ", "");

                    if (name.Length < 3)
                    {
                        name = n.Replace(" ", "");
                    }

                    return name?.Substring(0, Math.Min(4, name.Length - 1))?.ToUpper() ?? "";

                }
            }

            return null;
            
        }

        // GET: Stocks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stock stock = await db.Stocks.FindAsync(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stocks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<int> Create([Bind(Include = "ID,Name,Description,UnitID")] Stock stock)
        {
            if (ModelState.IsValid)
            {
                var _stockName = stock.Name;
                var stockCount = db.Stocks.Where(c => c.Name.ToUpper().Trim().Equals(_stockName.ToUpper().Trim())).Count();
                var count = ++stockCount;

                stock.StockNumber = CleanString(_stockName) + "-" + count;

                db.Stocks.Add(stock);
                return await db.SaveChangesAsync();
                
            }

            ViewBag.UnitID = new SelectList(db.Units, "ID", "UnitName", stock.UnitID);
            return 0;
        }


        // POST: Stocks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,StockNumber,Name,Description,UnitID")] Stock stock)
        {
            if (ModelState.IsValid)
            { 
                db.Entry(stock).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UnitID = new SelectList(db.Units, "ID", "UnitName", stock.UnitID);
            return View(stock);
        }

        // GET: Stocks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stock stock = await db.Stocks.FindAsync(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stocks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Stock stock = await db.Stocks.FindAsync(id);
            db.Stocks.Remove(stock);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
