﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.ViewModel;
using System.Linq;

namespace WebApplication.Controllers
{
    public class RequisitionDetailsViewModelController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        public ActionResult Index()
        {
            List<RequisitionDetailsViewModel> RequisitionDetails = new List<RequisitionDetailsViewModel>();

            var datalist = (from req in db.Requisitions
                            join req_det in db.RequisitionDetails on req.ID equals req_det.RequisitionID
                            select new { req.PurchaseOrderID, req_det.StockID, req_det.Quantity }).ToList();

            foreach (var item in datalist)
            {
                RequisitionDetailsViewModel rdvm = new RequisitionDetailsViewModel();
                rdvm.PurchaseOrderID = item.PurchaseOrderID;
                rdvm.StockID = item.StockID;
                rdvm.Quantity = item.Quantity;

                RequisitionDetails.Add(rdvm);
            }

            return View(RequisitionDetails);
        }
    }
}
    