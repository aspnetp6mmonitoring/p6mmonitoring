﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    public class PurchaseRequestsController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();

        [AjaxOnly]
        public PartialViewResult GetList()
        {
            ViewBag.CostCenters = db.CostCenters.ToList();
            return PartialView("_purchaserequestsList", db.PurchaseRequests.ToList());
        }

        // GET: PR
        public ActionResult Index()
        {
            ViewBag.CostCenterID = new SelectList(db.CostCenters, "ID", "Description");
            return View();
        }
        // GET: PurchaseRequests/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseRequest purchaseRequest = await db.PurchaseRequests.FindAsync(id);
            if (purchaseRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.PR_ID = id;
            return View(purchaseRequest);
        }

        // GET: PurchaseRequests/Create
        public ActionResult Create()
        {
            ViewBag.CostCenterID = new SelectList(db.CostCenters, "ID", "Description");
            return View();
        }

        // POST: PurchaseRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PrNumber,CostCenterID,Date")] PurchaseRequest purchaseRequest)
        {
            if (ModelState.IsValid)
            {
                db.PurchaseRequests.Add(purchaseRequest);
                await db.SaveChangesAsync();
                return RedirectToAction("Create");
            }

            ViewBag.CostCenterID = new SelectList(db.CostCenters, "ID", "Description", purchaseRequest.CostCenterID);
            return View(purchaseRequest);
        }

        // GET: PurchaseRequests/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseRequest purchaseRequest = await db.PurchaseRequests.FindAsync(id);
            if (purchaseRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.CostCenterID = new SelectList(db.CostCenters, "ID", "Description", purchaseRequest.CostCenterID);
            return View(purchaseRequest);
        }

        // POST: PurchaseRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PrNumber,CostCenterID,Date")] PurchaseRequest purchaseRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchaseRequest).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CostCenterID = new SelectList(db.CostCenters, "ID", "Description", purchaseRequest.CostCenterID);
            return View(purchaseRequest);
        }

        // GET: PurchaseRequests/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseRequest purchaseRequest = await db.PurchaseRequests.FindAsync(id);
            if (purchaseRequest == null)
            {
                return HttpNotFound();
            }
            return View(purchaseRequest);
        }

        // POST: PurchaseRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PurchaseRequest purchaseRequest = await db.PurchaseRequests.FindAsync(id);
            db.PurchaseRequests.Remove(purchaseRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
