﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using WebApplication.DAL;
using WebApplication.Models;
using WebApplication.Common;
using WebApplication.ViewModel;

namespace WebApplication.Controllers
{
    public class DeliveryDetailsController : Controller
    {
        private SupplyOfficeContext db = new SupplyOfficeContext();


        public async Task<ActionResult> Index(int? id)
        {
            Delivery delivery = await db.Deliveries.FindAsync(id);
            ViewBag.Deliveries = delivery;
            return View();
        }

        [AjaxOnly]
        public async Task<PartialViewResult> GetList(int? id1, int id2)
        {

           var viewModel1 = new PurchaseOrderDetailsVM();
           viewModel1.PurchaseOrders = db.PurchaseOrderDetails
                .Select(c => c.PurchaseOrder)
                .OrderBy(c => c.PoNumber);

            var viewModel2 = new DeliveryDetailsVM();
            viewModel2.PurchaseOrders = db.DeliveryDetails
                .Select(i => i.Delivery)
                .Select(i => i.PurchaseOrders);



            ViewBag.delID = id2;
            return PartialView("_purchaseorderdetails", db.PurchaseOrderDetails.Where(c => c.PurchaseOrderID == id1).ToList());
        }
        [AjaxOnly]
        public async Task<PartialViewResult> GetList1(int? id)
        {
            Delivery delivery = await db.Deliveries.FindAsync(id);
            ViewBag.Deliveries = delivery;
           
            return PartialView("_deliverydetails", db.DeliveryDetails.Where(c => c.DeliveryID == id).ToList());
        }
        



        // GET: DeliveryDetails/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryDetail deliveryDetail = await db.DeliveryDetails.FindAsync(id);
            if (deliveryDetail == null)
            {
                return HttpNotFound();
            }
            return View(deliveryDetail);
        }

        // GET: DeliveryDetails/Create
        public ActionResult Create()
        {
            ViewBag.DeliveryID = new SelectList(db.Deliveries, "ID", "ID");
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description");
            return View();
        }

        // POST: DeliveryDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        public async Task<ActionResult> CreatePODetails([Bind(Include = "ID,DeliveryID,StockID,Quantity")] DeliveryDetail deliveryDetail)
        {
            if (ModelState.IsValid)
            {
                
                    var ddetails = db.DeliveryDetails.FirstOrDefault(k => k.DeliveryID == deliveryDetail.DeliveryID && k.StockID == deliveryDetail.StockID);
                    if(ReferenceEquals(ddetails,null))
                    { 
                        db.DeliveryDetails.Add(deliveryDetail);
                        await db.SaveChangesAsync();
                    
                    }
                    else
                    {
                       
                                ddetails.Quantity = ddetails.Quantity + deliveryDetail.Quantity;
                                await db.SaveChangesAsync();
                         
                    }
                
                return RedirectToAction("Index", new { id = deliveryDetail.DeliveryID });
            }

            ViewBag.DeliveryID = new SelectList(db.Deliveries, "ID", "ID", deliveryDetail.DeliveryID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", deliveryDetail.StockID);
            return View(deliveryDetail);
        }

        // GET: DeliveryDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryDetail deliveryDetail = await db.DeliveryDetails.FindAsync(id);
            if (deliveryDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeliveryID = new SelectList(db.Deliveries, "ID", "ID", deliveryDetail.DeliveryID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", deliveryDetail.StockID);
            return View(deliveryDetail);
        }

        // POST: DeliveryDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,DeliveryID,StockID,Quantity")] DeliveryDetail deliveryDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliveryDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DeliveryID = new SelectList(db.Deliveries, "ID", "ID", deliveryDetail.DeliveryID);
            ViewBag.StockID = new SelectList(db.Stocks, "ID", "Description", deliveryDetail.StockID);
            return View(deliveryDetail);
        }

        // GET: DeliveryDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryDetail deliveryDetail = await db.DeliveryDetails.FindAsync(id);
            if (deliveryDetail == null)
            {
                return HttpNotFound();
            }
            return View(deliveryDetail);
        }

        // POST: DeliveryDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int id1)
        {
            DeliveryDetail deliveryDetail = await db.DeliveryDetails.FindAsync(id);
            db.DeliveryDetails.Remove(deliveryDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", new { id = id1 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
