﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class PurchaseOrderDetail
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Please choose Purchase Order Number")]
        public int? PurchaseOrderID { get; set; }

        [Display(Name = "Stock")]
        [Required(ErrorMessage = "Please choose stock item")]
        public int? StockID { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual Stock Stock { get; set; }
    }
}