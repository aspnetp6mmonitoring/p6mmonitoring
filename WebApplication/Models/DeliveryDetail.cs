﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{

    public class DeliveryDetail
    {
        [Key]
        public int ID { get; set; }

        public int? DeliveryID { get; set; }

        [Display(Name = "Stock")]
        [Required(ErrorMessage = "Please choose stock item")]
        public int? StockID { get; set; }


        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        public virtual Delivery Delivery { get; set; }
        public virtual Stock Stock { get; set; }

        public static implicit operator DeliveryDetail(Delivery v)
        {
            throw new NotImplementedException();
        }
    }
}