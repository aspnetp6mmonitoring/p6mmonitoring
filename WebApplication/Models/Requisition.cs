﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WebApplication.Models
{

    public class Requisition
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "R.I.S Number")]
        [Required(ErrorMessage = "R.I.S Number is required")]
        public string ReNumber { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public DateTime DateRecorded { get; set; } = DateTime.Now;
        public int? PurchaseOrderID { get; set; }
        
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual ICollection<RequisitionDetail> RequisitionDetails { get; set; }
    }
}