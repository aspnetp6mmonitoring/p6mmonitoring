﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class CostCenter
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Cost Center")]
        [Required(ErrorMessage = "Cost Center is required")]
        public string Description { get; set; }

        
        public virtual ICollection<PurchaseRequest> PurchaseRequests { get; set; }
    }
}