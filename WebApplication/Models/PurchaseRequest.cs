﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class PurchaseRequest
    {
        [Key]
        public int ID { get; set; }

        [MaxLength(9)]
        [Display(Name = "P.R. Number")]
        public string PrNumber { get; set; }

        public int? CostCenterID { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        
        public virtual CostCenter CostCenter { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrders { get; set;  }
    }
}