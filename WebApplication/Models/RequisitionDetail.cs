﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class RequisitionDetail
    {
        [Key]
        public int ID { get; set; }

        public int? RequisitionID { get; set; }

        [Display(Name = "Stock")]
        [Required(ErrorMessage = "Please choose stock item")]
        public int? StockID { get; set; }


        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        public virtual Requisition Requisition { get; set; }
        public virtual Stock Stock { get; set; }
    }
}