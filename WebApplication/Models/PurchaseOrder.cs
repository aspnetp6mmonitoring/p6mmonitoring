﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class PurchaseOrder
    {

        [Key]
        public int ID { get; set; }
        
        [Display(Name = "P.O. Number")]
        [Required(ErrorMessage = "P.O Number is required")]
        public string PoNumber { get; set; }

        [Display(Name = "Supplier Name")]
        [Required(ErrorMessage = "Please enter supplier name")]
        public string SupplierName { get; set; }

        [Display(Name = "Supplier Address")]
        [Required(ErrorMessage = "Please enter supplier address")]
        public string SupplierAddress { get; set; }

        [Display(Name = "Purpose")]
        [Required(ErrorMessage = "Purpose is required")]
        public string Purpose { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public DateTime DateRecorded { get; set; } = DateTime.Now;

        public int? PurchaseRequestID { get; set; }

        public virtual PurchaseRequest PurchaseRequest { get; set; }
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public virtual ICollection<Delivery> Deliveries { get; set; }
        public virtual ICollection<Requisition> Requisition { get; set; }
    }
}