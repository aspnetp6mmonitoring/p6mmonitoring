﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WebApplication.Models
{
    public class Delivery
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "P.O Number")]
        public int? PurchaseOrderID { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime DateRecorded { get; set; }
        
        public virtual PurchaseOrder PurchaseOrders { get; set; }
        public virtual ICollection<DeliveryDetail> DeliveryDetails { get; set; }
        
    }
}