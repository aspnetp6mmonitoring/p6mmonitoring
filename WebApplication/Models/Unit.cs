﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Unit
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Unit")]
        [Required(ErrorMessage = "Unit name is required")]
        [Index("IX_UnitName", 1, IsUnique = true)]
        public string UnitName { get; set; }

       public virtual ICollection<Stock> Stocks  { get; set; }
    }
}