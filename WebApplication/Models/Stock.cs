﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Stock
    {
        [Key]
        public int ID { get; set; }

        [ScaffoldColumn(false)]
        public string StockNumber { get; set; }

        [Display(Name = "Item Name")]
        [Required(ErrorMessage = "Item Name is required")]
        public string Name { get; set; }

        [Display(Name = "Item Description")]
        public string Description { get; set; }

        [NotMapped]
        public string GetDescription { get
            {
                return Description == "" ? Name?.ToUpper() : Name?.ToUpper()  + ", " + Description;
            }
        }

    
        [Display(Name = "Unit")]
        public int? UnitID { get; set; }

        public virtual Unit Unit { get; set; }
        public virtual ICollection<DeliveryDetail> DeliveryDetails { get; set; }
        public virtual ICollection<RequisitionDetail> RequisitionDetails { get; set; }
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
    }
}