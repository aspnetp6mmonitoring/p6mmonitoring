﻿SELECT D.ID,D.PurchaseOrderID,DD.StockID,S.Description,DD.Quantity FROM [Deliveries] D
JOIN [DeliveryDetails] DD ON DD.DeliveryID = D.ID
JOIN [STOCKS] S ON S.ID = DD.StockID