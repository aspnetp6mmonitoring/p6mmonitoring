﻿

select po.ID as POID,d.ID as DeliveryID, pod.ID as PurchaseOrderDetailsID,pod.StockID,pod.Quantity,
(select dd.Quantity from DeliveryDetails dd where dd.DeliveryID=d.ID and dd.StockID = pod.StockID) as DeliveredQuantity
from (PurchaseOrders po inner join Deliveries d on po.ID = d.PurchaseOrderID)
inner join PurchaseOrderDetails pod on (pod.PurchaseOrderID = po.ID)



select po.ID as POID,r.ID as RequisitionID,pod.ID as PurchaseOrderDetailsID,pod.StockID,pod.Quantity,
(select rd.Quantity from RequisitionDetails rd where rd.RequisitionID = r.ID and rd.StockID = pod.StockID) as RequestedQuantity
from (PurchaseOrders po inner join Requisitions r on po.ID = r.PurchaseOrderID)
inner join PurchaseOrderDetails pod on (pod.PurchaseOrderID = po.ID)